/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs")
const path = require("path")

const inputFilePath = path.join(__dirname, "lipsum.txt")
const outputFilePathUpperCase = path.join(__dirname, "lipsum-uppercase.txt")
const outputFilePathLowerCaseSentences = path.join(
    __dirname,
    "lipsum-lowercase-sentences.txt"
)
const outputFilePathSortedSentences = path.join(
    __dirname,
    "lipsum-lowercase-sentences-sorted.txt"
)
const filenamesFilePath = path.join(__dirname, "filenames.txt")

const readFile = (inputFilePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(inputFilePath, "utf-8", (err, data) => {
            if (err) {
                console.error(err)
                reject(err)
            } else {
                console.log("File read successfully")
                resolve(data)
            }
        })
    })
}

const convertToUpperCase = (data) => {
    return new Promise((resolve, reject) => {
        const upperCaseData = data.toUpperCase()
        fs.writeFile(outputFilePathUpperCase, upperCaseData, "utf8", (err) => {
            if (err) {
                console.error("Error writing to the new file:", err)
                reject(err)
            } else {
                console.log(
                    `Successfully wrote uppercase content to ${outputFilePathUpperCase}.`
                )
                addingFilesNames(filenamesFilePath, outputFilePathUpperCase)
                resolve(outputFilePathUpperCase)
            }
        })
    })
}

const convertToLowerCaseSentences = (data) => {
    return new Promise((resolve, reject) => {
        const lowerCaseData = data.toLowerCase().split(". ").join("\n")

        fs.writeFile(outputFilePathLowerCaseSentences, lowerCaseData, (err) => {
            if (err) {
                console.error("Error writing to the new file:", err)
                reject(err)
            } else {
                console.log(
                    `Successfully wrote lowercase content to ${outputFilePathLowerCaseSentences}.`
                )
                addingFilesNames(
                    filenamesFilePath,
                    outputFilePathLowerCaseSentences
                )
                resolve(outputFilePathLowerCaseSentences)
            }
        })
    })
}

const convertToSortedSentences = (data) => {
    return new Promise((resolve, reject) => {
        const lowerCaseData = data.split("\n").sort().join("\n")

        fs.writeFile(outputFilePathSortedSentences, lowerCaseData, (err) => {
            if (err) {
                console.error("Error writing to the new file:", err)
                reject(err)
            } else {
                console.log(
                    `Successfully wrote lowercase senstences sorted content to ${outputFilePathSortedSentences}.`
                )
                addingFilesNames(
                    filenamesFilePath,
                    outputFilePathSortedSentences
                )
                resolve(outputFilePathSortedSentences)
            }
        })
    })
}

const delteFilesAndClearContent = (data) => {
    return new Promise((resolve, reject) => {
        const fileLines = data.trim().split("\n")

        fileLines.map((filePath) => {
            const absoluteFilePath = path.resolve(filePath.trim())

            fs.unlink(absoluteFilePath, (err) => {
                if (err) {
                    console.error(`Error deleting ${absoluteFilePath}:`, err)
                    reject(err)
                } else {
                    console.log(`Deleted file: ${absoluteFilePath}`)
                }
            })
        })

        fs.writeFile(filenamesFilePath, "", "utf8", (err) => {
            if (err) {
                console.error("Error clearing filenames.txt:", err)
                reject(err)
            } else {
                console.log("Filenames file cleared.")
                resolve()
            }
        })
    })
}

// Store the name of the new file in filenames.txt
const addingFilesNames = (filenamesFilePath, fileName) => {
    fs.appendFile(filenamesFilePath, fileName + "\n", "utf8", (err) => {
        if (err) {
            console.error("Error appending to filenames.txt:", err)
            return
        } else {
            console.log(
                `Successfully stored the filename in ${filenamesFilePath}.`
            )
        }
    })
}

const fsProblem2 = () => {
    readFile(inputFilePath)
        .then((data) => {
            return convertToUpperCase(data)
        })
        .then((path) => {
            return readFile(path)
        })
        .then((data) => {
            return convertToLowerCaseSentences(data)
        })
        .then((path) => {
            return readFile(path)
        })
        .then((data) => {
            return convertToSortedSentences(data)
        })
        .then(() => {
            return readFile(filenamesFilePath)
        })
        .then((data) => {
            return delteFilesAndClearContent(data)
        })
        .catch((err) => {
            console.error(err)
        })
}

module.exports = fsProblem2

// const fsProblem2 = () => {
//     fs.readFile(inputFilePath, "utf8", (err, data) => {
//         if (err) {
//             console.error("Error reading the file:", err)
//         }
//         // Convert the content to uppercase
//         else {
//             const upperCaseData = data.toUpperCase()

//             // Write the uppercase content to a new file
//             fs.writeFile(
//                 outputFilePathUpperCase,
//                 upperCaseData,
//                 "utf8",
//                 (err) => {
//                     if (err) {
//                         console.error("Error writing to the new file:", err)
//                     } else {
//                         console.log(
//                             `Successfully wrote uppercase content to ${outputFilePathUpperCase}.`
//                         )
//                         addingFilesNames(
//                             filenamesFilePath,
//                             outputFilePathUpperCase
//                         )

//                         fs.readFile(
//                             outputFilePathUpperCase,
//                             "utf-8",
//                             (err, data) => {
//                                 if (err) {
//                                     console.error(
//                                         "Error reading the file:",
//                                         err
//                                     )
//                                 } else {
//                                     const lowerCaseData = data
//                                         .toLowerCase()
//                                         .split(". ")
//                                         .join("\n")

//                                     fs.writeFile(
//                                         outputFilePathLowerCaseSentences,
//                                         lowerCaseData,
//                                         (err) => {
//                                             if (err) {
//                                                 console.error(
//                                                     "Error writing to the new file:",
//                                                     err
//                                                 )
//                                             } else {
//                                                 console.log(
//                                                     `Successfully wrote lowercase content to ${outputFilePathLowerCaseSentences}.`
//                                                 )
//                                                 addingFilesNames(
//                                                     filenamesFilePath,
//                                                     outputFilePathLowerCaseSentences
//                                                 )

//                                                 fs.readFile(
//                                                     outputFilePathLowerCaseSentences,
//                                                     "utf-8",
//                                                     (err, data) => {
//                                                         if (err) {
//                                                             console.error(
//                                                                 "Error reading the file:",
//                                                                 err
//                                                             )
//                                                         } else {
//                                                             const sentences =
//                                                                 data
//                                                                     .split("\n")
//                                                                     .sort()
//                                                                     .join("\n")

//                                                             fs.writeFile(
//                                                                 outputFilePathSortedSentences,
//                                                                 sentences,
//                                                                 (err) => {
//                                                                     if (err) {
//                                                                         console.error(
//                                                                             "Error writing to the new file:",
//                                                                             err
//                                                                         )
//                                                                     } else {
//                                                                         console.log(
//                                                                             `Successfully wrote lowercase senstences sorted content to ${outputFilePathSortedSentences}.`
//                                                                         )
//                                                                         addingFilesNames(
//                                                                             filenamesFilePath,
//                                                                             outputFilePathSortedSentences
//                                                                         )

//                                                                         fs.readFile(
//                                                                             filenamesFilePath,
//                                                                             "utf8",
//                                                                             (
//                                                                                 err,
//                                                                                 data
//                                                                             ) => {
//                                                                                 if (
//                                                                                     err
//                                                                                 ) {
//                                                                                     console.error(
//                                                                                         "Error reading filenames.txt:",
//                                                                                         err
//                                                                                     )
//                                                                                 } else {
//                                                                                     // Split the file paths into an array of lines
//                                                                                     const fileLines =
//                                                                                         data
//                                                                                             .trim()
//                                                                                             .split(
//                                                                                                 "\n"
//                                                                                             )

//                                                                                     // Delete each file asynchronously
//                                                                                     fileLines.map(
//                                                                                         (
//                                                                                             filePath
//                                                                                         ) => {
//                                                                                             const absoluteFilePath =
//                                                                                                 path.resolve(
//                                                                                                     filePath.trim()
//                                                                                                 )

//                                                                                             fs.unlink(
//                                                                                                 absoluteFilePath,
//                                                                                                 (
//                                                                                                     err
//                                                                                                 ) => {
//                                                                                                     if (
//                                                                                                         err
//                                                                                                     ) {
//                                                                                                         console.error(
//                                                                                                             `Error deleting ${absoluteFilePath}:`,
//                                                                                                             err
//                                                                                                         )
//                                                                                                     } else {
//                                                                                                         console.log(
//                                                                                                             `Deleted file: ${absoluteFilePath}`
//                                                                                                         )
//                                                                                                     }
//                                                                                                 }
//                                                                                             )
//                                                                                         }
//                                                                                     )

//                                                                                     // Clear the contents of filenames.txt
//                                                                                     fs.writeFile(
//                                                                                         filenamesFilePath,
//                                                                                         "",
//                                                                                         "utf8",
//                                                                                         (
//                                                                                             err
//                                                                                         ) => {
//                                                                                             if (
//                                                                                                 err
//                                                                                             ) {
//                                                                                                 console.error(
//                                                                                                     "Error clearing filenames.txt:",
//                                                                                                     err
//                                                                                                 )
//                                                                                             } else {
//                                                                                                 console.log(
//                                                                                                     "Filenames file cleared."
//                                                                                                 )
//                                                                                             }
//                                                                                         }
//                                                                                     )
//                                                                                 }
//                                                                             }
//                                                                         )
//                                                                     }
//                                                                 }
//                                                             )
//                                                         }
//                                                     }
//                                                 )
//                                             }
//                                         }
//                                     )
//                                 }
//                             }
//                         )
//                     }
//                 }
//             )
//         }
//     })
// }
